import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import { Observable }     from 'rxjs/Observable';

import { Task } from './task';

@Injectable()
export class TaskService {

  private tasksUrl = 'http://homework.avantlink.com/tasks';
  private getHeaders: Headers;
  private headers: Headers;
  
  constructor(private http: Http) {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Application-ID', '170aa522-5c0c-11e6-9168-0a5449992ecf');
    this.getHeaders = new Headers();
    this.getHeaders.append('Application-ID', '170aa522-5c0c-11e6-9168-0a5449992ecf');
  }

  getTasks(): Promise<Task[]> {
    return this.http.get(this.tasksUrl, {headers: this.getHeaders})
               .toPromise()
               .then(response => response.json().data.map(this.toTask) as Task[])
               .catch(this.handleError);
  }
  
  private toTask(data: any): Task {
    let task = <Task>({
      id: data.task_id,
      name: data.task_name,
    });
    console.log(task);
    return task;
  }
  
  
  getTask(id: number): Promise<Task> {
    let url = `${this.tasksUrl}/${id}`;
    console.log('in getTasks(): ' + url);
    return this.http.get(url, {headers: this.getHeaders})
               .toPromise()
               .then(response => response.json().data as Task)
               .catch(this.handleError);
  }
  
  save(task: Task): Promise<Task>  {
    if (!task.id) {
      return this.post(task);
    }
    return this.put(task);
  }

  delete(task: Task) {
    let url = `${this.tasksUrl}/${task.id}`;

    return this.http
               .delete(url, {headers: this.getHeaders})
               .toPromise()
               .catch(this.handleError);
  }

  // Add new Task
  private post(task: Task): Promise<Task> {
    return this.http
               .post(this.tasksUrl, JSON.stringify(task), {headers: this.headers})
               .toPromise()
               .then(res => res.json().data.map(this.toTask))
               .catch(this.handleError);
  }

  // Update existing Task
  private put(task: Task) {
    let url = `${this.tasksUrl}/${task.id}`;
    return this.http
               .put(url, JSON.stringify(task), {headers: this.headers})
               .toPromise()
               .then(() => task)
               .catch(this.handleError);
  }
  
  private handleError(error: any) {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}