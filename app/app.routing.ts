import { Routes, RouterModule } from '@angular/router';
import { TasksComponent } from './tasks.component';
import { TaskDetailComponent } from './task-detail.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/tasks',
    pathMatch: 'full'
  },
  {
    path: 'tasks',
    component: TasksComponent
  },
  {
    path: 'task/:id',
    component: TaskDetailComponent
  }
];

export const routing = RouterModule.forRoot(appRoutes);
