import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing }       from './app.routing';

import { AppComponent }  from './app.component';
import { TasksComponent } from './tasks.component';
import { TaskDetailComponent } from './task-detail.component';

import { TaskService } from './task.service';

@NgModule({
  imports: [ 
    BrowserModule, 
    FormsModule,
    routing,
    HttpModule
  ],
  declarations: [ 
    TasksComponent,
    TaskDetailComponent
  ],
  providers: [
    TaskService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {}
