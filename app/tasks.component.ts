import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Task } from './task';
import { TaskService } from './task.service';

@Component({
  selector: 'my-tasks',
  templateUrl: 'app/tasks.component.html',
  styleUrls: ['app/tasks.component.css']

})
export class TasksComponent implements OnInit { 
  tasks: Task[];
  selectedTask: Task;
  addingTask = false;
  showSuccess = false;
  error: any;
  process: string;
  newTask: Task;
  
  constructor(private router: Router, private taskService: TaskService) {}
    
  getTasks() {
    this.taskService
        .getTasks()
        .then(tasks => this.tasks = tasks)
        .catch(error => this.error = error);
  }
  
  addTask() {
    this.addingTask = true;
    this.selectedTask = null;
    this.newTask = new Task();
  }
  
  save(taskToSave: Task) {
    this.showSuccess = false;
    var isNew = false;
    if (!taskToSave.id) {
      isNew = true;
    }
    this.taskService
        .save(taskToSave)
        .then(task => {
          console.log(task);
          if (isNew) {
             this.tasks.push(task);
             this.process = 'created';
          } else {
            this.process = 'updated';
          }
          this.showSuccess = true;
          this.selectedTask = null;
        })
        .catch(error => this.error = error); // TODO: Display error message
  }

  close(savedTask: Task) {
    this.addingTask = false;
    if (savedTask) { this.getTasks(); }
  }
  
  deleteTask(task: Task, event: any) {
    this.process = 'deleted';
    if (event) event.stopPropagation();
    this.taskService
        .delete(task)
        .then(res => {
          this.tasks = this.tasks.filter(h => h !== task);
          if (this.selectedTask === task) { this.selectedTask = null; }
          this.showSuccess = true;
        })
        .catch(error => this.error = error);
  }
  
  ngOnInit() {
    this.getTasks();
  }
    
  onSelect(task: Task) {
    this.showSuccess = false;
    this.selectedTask = task;
    this.addingTask = false;
  }
  
  gotoDetail() {
    this.router.navigate(['/task', this.selectedTask.id]);
  }
}
